﻿using System.Collections.Generic;

namespace Yes.Infrastructure.Common.Models
{
	public class ErrorModel : JsonModel
	{
		public Dictionary<string, string> Errors { get; set; }

		public string Message { get; set; }
		
		public int ErrorCode { get; set; }
	}
}
