﻿using Newtonsoft.Json;

namespace Yes.Infrastructure.Common.Models
{
	public abstract class JsonModel
	{
		public override string ToString()
		{
			return JsonConvert.SerializeObject(this);
		}
	}
}
