﻿using Newtonsoft.Json;

namespace Yes.Infrastructure.Common.Extensions
{
    public static class ObjectExtensions
    {
        public static string ToJsonString(this object value)
        {
            return JsonConvert.SerializeObject(value);
        }
    }
}
