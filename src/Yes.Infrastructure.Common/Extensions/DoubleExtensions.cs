﻿namespace Yes.Infrastructure.Common.Extensions
{
    public static class DoubleExtensions
    {
        /// <summary>
        /// Возвращает строку с округлением до десятых
        /// </summary>		
        /// <returns>строка</returns>
        public static string Round(this double number)
        {
            return $"{number:0.0}";
        }
    }
}
