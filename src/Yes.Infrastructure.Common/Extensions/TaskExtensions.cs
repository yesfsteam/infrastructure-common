﻿using System.Threading.Tasks;

namespace Yes.Infrastructure.Common.Extensions
{
    public static class TaskExtensions
    {
        public static T GetSynchronousResult<T>(this Task<T> task)
        {
            return Task.Run(async () => await task).ConfigureAwait(false).GetAwaiter().GetResult();
        }

        public static void GetSynchronousResult(this Task task)
        {
            Task.Run(async () => await task).ConfigureAwait(false).GetAwaiter().GetResult();
        }
    }
}
