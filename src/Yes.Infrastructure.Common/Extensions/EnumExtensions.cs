﻿using System;
using System.ComponentModel;

namespace Yes.Infrastructure.Common.Extensions
{
    public static class EnumExtensions
    {
        public static string GetDefaultValue(this Enum en)
        {
            var memInfo = en.GetType().GetMember(en.ToString());

            if (memInfo != null && memInfo.Length > 0)
            {
                var attrs = memInfo[0].GetCustomAttributes(typeof(DefaultValueAttribute), false);

                if (attrs != null && attrs.Length > 0)
                {
                    return ((DefaultValueAttribute)attrs[0]).Value.ToString();
                }
            }

            return string.Empty;
        }

        public static string GetDescription(this Enum en)
        {
            if (en == null)
                return string.Empty;

            var type = en.GetType();

            var memInfo = type.GetMember(en.ToString());

            if (memInfo != null && memInfo.Length > 0)
            {
                var attrs = memInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);

                if (attrs != null && attrs.Length > 0)
                {
                    return ((DescriptionAttribute)attrs[0]).Description;
                }
            }

            return en.ToString();
        }
    }
}
