﻿using System;

namespace Yes.Infrastructure.Common.Extensions
{
    public static class DateTimeExtensions
    {
        /// <summary>
        /// Возвращает время пройденное от заданной даты до текущей
        /// </summary>		
        /// <returns>строка</returns>
        public static string GetDuration(this DateTime beginTime)
        {
            return $"{(DateTime.Now - beginTime).TotalSeconds.Round()} sec.";
        }
    }
}
