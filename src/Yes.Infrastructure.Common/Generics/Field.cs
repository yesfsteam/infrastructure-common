﻿using System.Collections.Generic;

namespace Yes.Infrastructure.Common.Generics
{
	public class Field<T>
	{
		public Field()
		{
		}

		public Field(T value)
		{
			Value = value;
		}

		public T Value { get; set; }

		public bool Enabled { get; set; }

		public static implicit operator Field<T>(T value) => new Field<T>(value);
		
		protected bool Equals(Field<T> other)
		{
			return EqualityComparer<T>.Default.Equals(Value, other.Value) && Enabled == other.Enabled;
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != this.GetType()) return false;
			return Equals((Field<T>) obj);
		}

		public override int GetHashCode()
		{
			return Value.GetHashCode();
		}
	}

	public static class FieldExtension
	{
		public static bool HasValue(this object @object)
		{
			return @object != null;
		}
	}
}